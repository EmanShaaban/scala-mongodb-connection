name := "MongoDB Example"

version := "1.0"

scalaVersion := "2.11.6"


libraryDependencies ++= Seq(
    "org.mongodb" %% "casbah" % "3.1.1",
    "org.slf4j" % "slf4j-simple" % "1.7.21"
)

scalacOptions += "-deprecation"
