import com.mongodb.casbah.Imports._

case class Stock (symbol: String, price:Double)

object Common {

  def buildMongoDBObject(stock: Stock) : MongoDBObject = {

    val builder = MongoDBObject.newBuilder
    builder += "symbol" -> stock.symbol
    builder += "price" -> stock.price
    builder.result
  }

}
