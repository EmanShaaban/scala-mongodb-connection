import com.mongodb.casbah.Imports._
import Common._


object Insert extends App{

  val apple = Stock("APL" ,50)
  val google = Stock("GOO",60)
  val netflix = Stock("NFLX", 70)

saveStock(apple)
saveStock(google)
saveStock(netflix)

  def saveStock(stock: Stock) {
    val mongoObj = buildMongoDBObject(stock)
    MongoFactory.collection.save(mongoObj)
  }


}
